import React, { Component } from 'react'
import './App.css'
import Header from './components/Header'
import RealtimePanel from './components/RealtimePanel'
import OverviewPanel from './components/OverviewPanel'
import bg from './assets/background.png'
import { observer } from 'mobx-react'
import { action, computed, observable } from 'mobx'
import { getEnergy, getPower } from './api/solaredge'
import { getPrice, getTotalPrice } from './api/touService'
import moment from 'moment'
import config from './config'
import setData from './helper/setData'

class AppState {
  @observable currentTime = moment()
  lastUpdated = this.currentTime
  @observable totalEnergy = 0
  @observable dailyEnergy = 0
  @observable monthlyEnergy = 0
  @observable totalEarned = 0
  @observable dailyEarned = 0
  @observable monthlyEarned = 0
  @observable realtimeGraphData = []
  @observable useRealtimeGraph = true
  @observable date = this.currentTime.format('YYYY-MM-DD')

  constructor() {
    // this.getTotalEnergy()
    // this.getSavedMoneyEarned()
    // this.getPowerGraph(this.currentTime.format('YYYY-MM-DD'))
    // setInterval(() => {
    //   this.currentTime = moment()
    //   if (this.currentTime.subtract(15, 'minutes').isAfter(this.lastUpdated)) {
    //     this.lastUpdated.add(15, 'minutes')
    //     this.getTotalEnergy()
    //     this.getSavedMoneyEarned()
    //     this.getPowerGraph()
    //   }
    // }, 1000)
  }

  @action
  getSavedMoneyEarned() {
    const currentDate = this.currentTime.format('YYYY-MM-DD')
    getPrice(this.date).then(data => {
      const { daily_price, monthly_price } = data
      this.dailyEarned = Math.floor(daily_price)
      this.monthlyEarned = Math.floor(monthly_price)
    })
    getTotalPrice(currentDate).then(data => {
      const { total_price } = data
      this.totalEarned = Math.floor(total_price)
    })
  }

  @action
  getTotalEnergy() {
    const date = this.currentTime.format('YYYY-MM-DD')
    getEnergy(date, date, 'DAY').then(({ energy }) => {
      const [data = { value: 0 }] = energy.values
      this.dailyEnergy = Math.floor(data.value / 1000)
    })

    getEnergy(date, date, 'MONTH').then(({ energy }) => {
      const [data = { value: 0 }] = energy.values
      this.monthlyEnergy = Math.floor(data.value / 1000)
    })

    getEnergy(config.startDate, date, 'YEAR').then(({ energy }) => {
      this.totalEnergy = Math.floor(
        energy.values.reduce((data, sum) => sum.value + data.value) / 1000
      )
    })
  }

  @action
  getPowerGraph(date) {
    // FIXME: Check and set realtime here
    if (this.useRealtimeGraph) {
      getPower(date).then(({ powerDetails }) => {
        this.realtimeGraphData = setData(powerDetails)
      })
    }
  }

  @action
  setUseRealTimeGraph(useRealtimeGraph, date) {
    this.useRealtimeGraph = useRealtimeGraph
    this.date = date
  }

  @action
  setPreviousDataGraph(date) {
    getPower(date).then(({ powerDetails }) => {
      this.realtimeGraphData = setData(powerDetails)
    })
  }

  @computed get coEmissionsAvoided() {
    return Math.floor((this.totalEnergy * 0.53) / 907.185)
  }

  @computed get operationDays() {
    return this.currentTime.diff(config.startDate, 'days')
  }
}

const Clock = observer(({ appState }) => (
  <div
    style={{
      position: 'absolute',
      right: 4,
      bottom: 4
    }}
  >
    {appState.currentTime.toString()}
  </div>
))

class App extends Component {
  constructor(props) {
    super(props)
    this.appState = new AppState()
  }

  render() {
    return (
      <div style={{ background: `url(${bg})` }}>
        <Header />
        {/* <RealtimePanel appState={this.appState} /> */}
        <OverviewPanel appState={this.appState} />
        {/* <Clock appState={this.appState} /> */}
      </div>
    )
  }
}

export default App
