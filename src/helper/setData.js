import groupBy from 'lodash/groupBy'
import moment from 'moment'

const types = ['Consumption', 'Production', 'SelfConsumption']
const setData = data => {
  const dateGroups = groupBy(data.meters[0].values, data => data.date)
  const graphData = Object.keys(dateGroups).map((dateGroup, index) => {
    let returnData = {}
    types.forEach(type => {
      let meterData = data.meters.find(meter => meter.type === type)
      returnData[type] = meterData.values[index].value || 0
    })
    return { date: moment(dateGroup).format('HH:mm'), ...returnData }
  })
  return graphData
}

export default setData
