import React from 'react'

const EnergyBox = ({ title, baht, energy }) => {
  return (
    <div
      style={{
        padding: 15,
        maxWidth: 180,
        maxHeight: 190,
        background: '#e7e7e7',
        textAlign: 'center',
        borderRadius: 10
      }}
    >
      <p style={{ fontSize: 18, fontWeight: 600, margin: 0 }}>{title}</p>
      <div style={{ padding: 10, background: 'white', marginTop: 10 }}>
        <p
          style={{ color: '#231e50', fontWeight: 700, fontSize: 22, margin: 0 }}
        >
          {baht}
        </p>
      </div>
      <p
        style={{
          color: '#818181',
          fontWeight: 400,
          fontSize: 14,
          margin: 0,
          marginTop: 5
        }}
      >
        Baht
      </p>
      <div style={{ padding: 10, background: 'white', marginTop: 12 }}>
        <p
          style={{ color: '#231e50', fontWeight: 700, fontSize: 22, margin: 0 }}
        >
          {energy}
        </p>
      </div>
      <p
        style={{
          color: '#818181',
          fontWeight: 400,
          fontSize: 14,
          margin: 0,
          marginTop: 5
        }}
      >
        kWh
      </p>
    </div>
  )
}

export default EnergyBox
