import React from 'react'
import realtimeHeader from '../assets/realtime-header.png'
import moment from 'moment'

import AreaChart from './AreaChart'
import EnergyBox from './EnergyBox'
import DateSelect from './DateSelect'

//api
import { observer } from 'mobx-react'

import { toJS } from 'mobx'
import config from '../config'

const RealTimePanel = observer(props => {
  const { appState } = props
  const types = ['Consumption', 'Production', 'SelfConsumption']

  const changeDate = date => {
    if (moment(date).isSame(appState.currentTime.format('YYYY-MM-DD'), 'day')) {
      appState.setUseRealTimeGraph(true, date)
    } else {
      appState.setUseRealTimeGraph(false, date)
      appState.setPreviousDataGraph(date)
    }
  }

  const onClickSelect = type => {
    if (type === 'increase') {
      let newDate = moment(appState.date, 'YYYY-MM-DD')
        .add(1, 'days')
        .format('YYYY-MM-DD')
      if (
        moment(newDate).isSame(appState.currentTime.format('YYYY-MM-DD'), 'day')
      ) {
        appState.setUseRealTimeGraph(true, newDate)
        appState.setPreviousDataGraph(newDate)
      }
      if (moment(newDate).isBefore(moment())) {
        appState.setUseRealTimeGraph(false, newDate)
        appState.setPreviousDataGraph(newDate)
      }
    } else {
      let newDate = moment(appState.date, 'YYYY-MM-DD')
        .subtract(1, 'days')
        .format('YYYY-MM-DD')
      appState.setUseRealTimeGraph(false, newDate)
      appState.setPreviousDataGraph(newDate)
    }
  }
  return (
    <div style={{ height: 687 }}>
      <img src={realtimeHeader} alt={'realtime-header'} width={'40%'} />
      <div
        style={{
          background: 'white',
          height: '85%',
          margin: 30,
          marginTop: -15,
          borderRadius: '0px 30px 30px',
          paddingTop: 30,
          paddingLeft: 15,
          paddingRight: 15
        }}
      >
        {toJS(appState.realtimeGraphData).length > 0 && (
          <div style={{ display: 'flex' }}>
            <div>
              <p style={{ marginTop: 0, marginLeft: 20 }}>W</p>
              <AreaChart
                data={toJS(appState.realtimeGraphData)}
                dataKeys={types}
                color={['#f27073', '#1aae51', '#6dbff9']}
                keyword={'date'}
              />
            </div>
            <div>
              <div
                style={{ display: 'flex', marginTop: 20, alignItems: 'center' }}
              >
                <p>Date :</p>
                <input
                  type="date"
                  id="dt"
                  onChange={event => changeDate(event.target.value)}
                  style={{
                    marginLeft: 15,
                    fontSize: 15,
                    border: '1px solid #e5e5e7'
                  }}
                  value={appState.date}
                  max={moment().format('YYYY-MM-DD')}
                  min={config.startDate}
                />
              </div>
              <div style={{ paddingTop: 10, paddingLeft: 20 }}>
                <EnergyBox
                  title={'Daily'}
                  baht={appState.dailyEarned}
                  energy={appState.dailyEnergy}
                />
              </div>
              <div style={{ paddingTop: 15, paddingLeft: 20 }}>
                <EnergyBox
                  title={'Monthly'}
                  baht={appState.monthlyEarned}
                  energy={appState.monthlyEnergy}
                />
              </div>
              <div style={{ paddingTop: 12, paddingLeft: 20 }}>
                <DateSelect
                  onClickSelect={onClickSelect}
                  date={appState.date}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  )
})

RealTimePanel.propTypes = {}

export default RealTimePanel
