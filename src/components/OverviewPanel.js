import React from 'react'
import PropTypes from 'prop-types'
import plantOverviewBg from '../assets/plant-overview-bg.png'
import PlantPanel from './PlantPanel'
import EnergyBox from './EnergyBox'
import { observer } from 'mobx-react'
import PieChart from './PieChart'
import SummaryPanel from './SummaryPanel'

const data = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 }
]
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042']

const OverviewPanel = observer(({ appState }) => {
  return (
    <div style={{ height: 1018 }}>
      <div
        style={{
          height: 572,
          background: `url(${plantOverviewBg})`,
          backgroundRepeat: 'round'
        }}
      >
        <div style={{ display: 'flex' }}>
          <PlantPanel />
          <div style={{ display: 'flex' }}>
            <div>
              <p>RealTime graph</p>
              <div style={{ display: 'flex' }}>
                <EnergyBox title={'Monthly'} baht={123456} energy={123456} />
                <EnergyBox title={'Daily'} baht={123456} energy={123456} />
              </div>
            </div>
            <div>
              <PieChart data={data} colors={COLORS} />
            </div>
          </div>
        </div>
      </div>
      <div>
        <SummaryPanel />
      </div>
    </div>
  )
})

OverviewPanel.propTypes = {}

export default OverviewPanel
