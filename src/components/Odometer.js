import React from 'react'
import PropTypes from 'prop-types'

const convertToDigits = (number, digits = 5) => {
  const numberArray = number.toString().split('')
  while (numberArray.length < digits) {
    numberArray.unshift('0')
  }
  return numberArray
}

const Odometer = props => {
  const { value = 0, digits } = props
  return (
    <div style={{
      backgroundColor: 'rgb(0, 0, 0)',
      border: '.25em solid rgb(40, 40, 40)',
      boxShadow: '0px 0px .1em .1em rgba(0,0,0,0.75)',
      display: 'inline-flex',
      fontFamily: 'Arial, sans-serif',
      fontSize: 20,
      height: '1.5em',
      padding: '0 .05em',
      position: 'relative',
      zIndex: 0
    }}>
      {convertToDigits(value, digits).map((digit, index) =>
        <div
          key={index}
          style={{
            backgroundColor: '#3c3c3c',
            color: 'white',
            margin: '0 .05em',
            height: '1.2em',
            overflow: 'hidden',
            fontSize: '1.3em',
            perspective: 1000,
            position: 'relative',
            width: '0.75em'
          }}>
          <div style={{
            alignItems: 'center',
            display: 'flex',
            height: '100%',
            justifyContent: 'center',
            position: 'absolute',
            width: '100%',
            transform: 'drumTransform(0)',
            zIndex: 0
          }}>
            {digit}
          </div>
        </div>
      )}
    </div>
  )
}

Odometer.propTypes = {}

export default Odometer
