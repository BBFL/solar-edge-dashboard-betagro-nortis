import React from 'react'
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts'

const DataFormater = number => {
  if (number > 1000000000) {
    return (number / 1000000000).toString() + 'B'
  } else if (number > 1000000) {
    return (number / 1000000).toString() + 'M'
  } else if (number > 1000) {
    return (number / 1000).toString() + 'K'
  } else {
    return number.toString()
  }
}

const Chart = ({ data, dataKeys, color, keyword }) => {
  return (
    <AreaChart
      width={755}
      height={520}
      data={data}
      margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey={keyword} />
      <YAxis tickFormatter={DataFormater} />
      <Tooltip />
      {dataKeys.map((dataKey, index) => (
        <Area
          type="monotone"
          dataKey={dataKey}
          stroke={color[index]}
          fill={color[index]}
        />
      ))}
      <Legend iconType="square" />
    </AreaChart>
  )
}
export default Chart
