import React from 'react'
import moment from 'moment'
import config from '../config'

// FIXME: Use appState is Realtime
const checkDateIsSame = (firstDate, secondDate) => {
  return moment(firstDate).isSame(secondDate)
}

const DateSelect = ({ onClickSelect, date }) => {
  return (
    <div style={{ display: 'flex' }}>
      <p
        style={{
          fontSize: 13,
          color: checkDateIsSame(config.startDate, date) ? 'grey' : '#4199bc',
          pointerEvent: checkDateIsSame(config.startDate, date)
            ? 'none'
            : 'auto'
        }}
        onClick={() => onClickSelect('decrease')}
      >
        &larr; Previous day
      </p>
      <p
        style={{
          fontSize: 13,
          color: '#4199bc',
          marginLeft: 10,
          marginRight: 10
        }}
      >
        |
      </p>
      <p
        style={{
          fontSize: 13,
          color: checkDateIsSame(moment().format('YYYY-MM-DD'), date)
            ? 'grey'
            : '#4199bc',
          pointerEvent: checkDateIsSame(config.startDate, date)
            ? 'none'
            : 'auto'
        }}
        onClick={() => onClickSelect('increase')}
      >
        Next day &rarr;
      </p>
    </div>
  )
}

export default DateSelect
